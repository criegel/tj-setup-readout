from Keithley import KeithleySMU2400Series
import yaml

configuration_file = ''
with open('config_keithley_tj.yaml', 'r') as file:
    configuration_file = yaml.load(file)

vsub = KeithleySMU2400Series(configuration_file)

vsub.disable_output()
vsub.set_voltage(0 ,unit='V')

