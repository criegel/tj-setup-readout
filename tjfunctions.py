#
# ------------------------------------------------------------
# Christian Riegel
# CERN / University of Wuppertal
# 27/11/2015
# Functions for the TJ-INVESTIGATOR setup
# Using pyBAR and Basil
# ------------------------------------------------------------
#


import time
from bitarray import bitarray
from basil.dut import Dut


def rampVdd(dut, p_steps=5):
  dut['gpac'].set_current_limit('PWR0',70,unit='mA')
  dut['Vdd'].set_enable(False)
  dut['Vreset'].set_enable(False)
  for i in range(p_steps):
    voltage = float(1.0 + i*0.2)
    dut['Vdd'].set_voltage(voltage, unit='V')
    dut['Vdd'].set_enable(True)
    dut['Vreset'].set_enable(True)
    print("Vdd set to "+str(voltage))
    print("Current current is "+str(dut['Vdd'].get_current(unit="mA")))
    time.sleep(1)

def switchVdd(dut, p_state):
  dut['Vdd'].set_enable(bool(p_state))

def on(dut):
  rampVdd(dut,5)

def off(dut):
  rampVdd(dut,0)

def shutDown(dut):
  switchVdd(dut,False)

def resetV(dut, p_vreset):
  dut['Vreset'].set_voltage(p_vreset, unit='V')
  dut['Vreset'].set_enable(True)
  print("Reset voltage set to: "+str(dut['Vreset'].get_voltage())+" V")

def setMatrix(dut, p_matrix):
  dut["Gpio"]["Matrix"] = p_matrix
#  dut["Gpio"]["LED"] = 3
  dut["Gpio"].write()
  print("Selection matrix set to: "+str(dut["Gpio"]["Matrix"]))

def setInjPulse(dut,Vlow,Vhigh,tdelay,twidth):
  dut['VPulse_low'].set_voltage(Vlow, unit='V')
  dut['VPulse_high'].set_voltage(Vhigh, unit='V')
  dut['Inj_Pulse'].DELAY = tdelay
  dut['Inj_Pulse'].WIDTH = twidth
  print("Pulse delay time set to: "+str(dut['Inj_Pulse'].DELAY)+" clock cycles")
  print("Pulse width time set to: "+str(dut['Inj_Pulse'].WIDTH)+" clock cycles") 
  print("Pulse repetitions set to: "+str(dut['Inj_Pulse'].REPEAT))

def startReset(dut, p_nrepeat):
  dut['Inj_Pulse'].REPEAT = p_nrepeat
  dut['Inj_Pulse'].EN = True
  dut['Inj_Pulse'].start()

def probeReset(p_sleep=1):
  for i in range(10):
    voltage = float(0.1+i*0.1)
    resetV(float(voltage))
    time.sleep(float(p_sleep))

def limitI(dut, p_lim):
  dut['gpac'].set_current_limit('PWR0',p_lim,unit='mA')
  print("Current limit of PWR0 set to "+str(p_lim)+" mA")

def current(dut):
  print("Current current is "+str(dut['Vdd'].get_current(unit="mA")))

def ifolCurrents(dut):
  print("\n\nReading the four currents:\n")
  for i in range(4):
	  print("Current I"+str(i)+" set to: "+str(icurrents[i])+" uA and read back: "+str(dut['Current'+str(i)].get_current(unit="uA"))+" uA\n")

def setPulsing(dut, p_active):
	if(p_active):
		dut["Gpio"]["LED"] = ( dut["Gpio"]["LED"] | bitarray('00010') )
	else:
		dut["Gpio"]["LED"] = ( dut["Gpio"]["LED"] & bitarray('11101') )
	dut["Gpio"].write()
	print("Pulsing set to: "+str(p_active))

def setReset(dut, p_active):
	if(p_active):
		dut["Gpio"]["LED"] = ( dut["Gpio"]["LED"] | bitarray('00001') )
	else:
		dut["Gpio"]["LED"] = ( dut["Gpio"]["LED"] & bitarray('11110') )
	dut["Gpio"].write()
	print("Reset set to: "+str(p_active))

def setLed(dut, p_bit):
	dut["Gpio"]["LED"] = p_bit
	dut["Gpio"].write()
