import serial
from serial import Serial
import time
import yaml

# :-)
Units = { 
          'Voltage' :
          { 'mV' : 0.001,
             'V' : 1.0},
         
          'Current' :
          { 'nA' : 0.000000001,
            'uA' : 0.000001,
            'mA' : 0.001,
             'A' : 1.0}
         }

Modi = {
         'Source' :
         { 'V' : 'VOLT',
            'v' : 'VOLT',
            'A' : 'CURR',
            'a' : 'CURR'
         },
         'Measure' :
         { 'V' : 'VOLT',
          'v' : 'VOLT',
           'A' : 'CURR',
           'a' : 'CURR'}
         }

    
#Class for the Keithley SMU 2400/2410 series
class KeithleySMU2400Series:
    ser = None
    
    def __init__(self, conf):
        self.configuration_file = conf
        self.set_device_configuration()
    
    #===========================================================================
    # Open serial interface
    #===========================================================================
    def open_device_interface(self):
        self._ser.open()
        print ("Device Ready at Port %s"%(self.configuration_file["Device"]["Configuration"]["Port"]))
    
    #===========================================================================
    # Switch on the output
    #===========================================================================
    def enable_output(self):
        self._ser.write(':OUTPUT ON\r\n')
        print ("Output On")
    
    def disable_output(self):
        self._ser.write(':OUTPUT OFF\r\n')
        print ("Output Off")

        # short commands for interpreter
    def on(self):
        for x in range(0,2):
            self.enable_output()
            time.sleep(0.5)

    def off(self):
        for x in range(0,2):
            self.disable_output()
            time.sleep(0.5)
    
    #===========================================================================
    # Close serial interface
    #===========================================================================
    def close_device_interface(self):
        self._ser.close()
        print ("Device Closed at Port %s"%(self.configuration_file["Device"]["Configuration"]["Port"]))
    
    
    def set_device_configuration(self):
        #Initialization of the Serial interface
        try:
            self._ser = Serial(
                                 port = self.configuration_file["Device"]["Configuration"]["Port"],
                                 baudrate = self.configuration_file["Device"]["Configuration"]["Baudrate"],
                                 timeout = 2,
                                 parity = 'E'
                                 )
            self._source = Modi['Source'][self.configuration_file["Device"]["Configuration"]["Source"]]
            self._measure = Modi['Measure'][self.configuration_file["Device"]["Configuration"]["Measure"]]    
            
            #Specifies the size of data buffer
            self._triggerCount = self.configuration_file["Device"]["Configuration"]["TriggerCount"]
            
            #Specifies trigger delay in seconds
            self._triggerDelay = self.configuration_file["Device"]["Configuration"]["TriggerDelay"]
            
            
            self._autorangeMeasure = self.configuration_file["Device"]["Configuration"]["AutoRangeMeasure"]
            self._autorangeSource = self.configuration_file["Device"]["Configuration"]["AutoRangeSource"]
            
            self._complianceSource = self.configuration_file["Device"]["Configuration"]["ComplianceSource"] 
            self._complianceMeasure = self.configuration_file["Device"]["Configuration"]["ComplianceMeasure"]
            
            self._sourceRange = self.configuration_file["Device"]["Configuration"]["RangeSource"]
            
            
            #Setup the source
            self._ser.write('*RST\r\n')
            
            self._ser.write(':SYST:BEEP:STAT OFF\r\n')

            self._ser.write(':SOUR:CLEar:IMMediate\r\n')
            self._ser.write(':SOUR:FUNC:MODE %s\r\n'%(self._source))
            #self._ser.write(':SOUR:CLEar:IMMediate\r\n')
            self._ser.write(':SOUR:%s:MODE FIX\r\n'%(self._source))
            self._ser.write(':SOUR:%s:RANG:AUTO %s\r\n'%(self._source, self._autorangeSource))
            self._ser.write(':SOUR:%s:PROT:LEV %s\r\n'%(self._source, self._complianceSource))
            if(self._autorangeSource == 'OFF'):
                self._ser.write(':SOUR:%s:RANG %s\r\n'%(self._source, self._sourceRange))
            else:
                None
            
            #Setup the sensing
            self._ser.write(':SENS:FUNC \"%s\"\r\n'%(self._measure))
            self._ser.write(':SENS:%s:PROT:LEV %s\r\n'%(self._measure, self._complianceMeasure))
            self._ser.write(':SENS:%s:RANG:AUTO %s\r\n'%(self._measure, self._autorangeMeasure))
            
            #Setup the buffer
            self._ser.write(':TRAC:FEED:CONT NEVer\r\n')
            self._ser.write(':TRAC:FEED SENSE\r\n')  
            self._ser.write(':TRAC:POIN %s\r\n'%(self._triggerCount))
            self._ser.write(':TRAC:CLEar\r\n')
            self._ser.write(':TRAC:FEED:CONT NEXT\r\n')                
            
            #Setup the data format
            self._ser.write(':FORMat:DATA ASCii\r\n')
            self._ser.write(':FORMat:ELEM VOLTage, CURRent\r\n')
             
            #Setup the trigger
            self._ser.write(':TRIG:COUN %s\r\n'%(self._triggerCount))
            self._ser.write(':TRIG:DELay %s\r\n'%(self._triggerDelay))
            
            print ("Device at Port %s Configured"%(self.configuration_file["Device"]["Configuration"]["Port"]))
        
        except ValueError:
            print('ERROR: No serial connection. Chech cable!')
        
    
    def enable_auto_range(self):
        self._ser.write(':SENS:RANG:AUTO ON\r\n')
       
    def disable_auto_range(self):
        self._ser.write(':SENS:RANG:AUTO OFF\r\n')
    
    def reset(self):
        self._ser.write('*RST\r\n')
       
    def set_value(self, source_value):
        if(source_value > self._complianceSource):
            print("ERROR: Source value is higher than Compliance!")
        else:
            self._ser.write(':SOUR:%s:LEVel %s\r\n'%(self._source, source_value))
            time.sleep(self.configuration_file["Device"]["Configuration"]["SettlingTime"])
      
    def set_voltage(self, voltage_value, unit):
        val = voltage_value*Units['Voltage'][unit]
        if(val>0):
            print "Please do not set positive VSUB: " + str(val)
            return val
        elif(val<-8):
            print "Please do not set too low VSUB: " + str(val)
            return val
        else:
            if(val > self._complianceSource):
                print("ERROR: Source value is higher than Compliance!")
            else:

                val = voltage_value*Units['Voltage'][unit]
                self._ser.write(':SOUR:%s:LEVel %s\r\n'%(self._source, val))
                time.sleep(self.configuration_file["Device"]["Configuration"]["SettlingTime"])
                print "Output voltage set to " + str(val)
        
    def set_source_upper_range(self, senseUpperRange):
        self._ser.write(':SENSE:%s:RANG:UPP %s\r\n'%(self._source, senseUpperRange))
          
    def sample(self):
        self._ser.write(':TRAC:FEED:CONT NEVer\r\n')
        self._ser.write(':TRACe:CLEar\r\n')
        self._ser.write(':TRAC:FEED:CONT NEXT\r\n')
        self._ser.write(':INIT\r\n')
    
    def get_raw_values(self):
        self._ser.write(':TRACe:DATA?\r\n')
       
    def get_mean(self): 
        self._ser.write(':CALC3:FORM MEAN\r\n')
        self._ser.write(':CALC3:DATA?\r\n')
     
    def get_std(self):
        self._ser.write(':CALC3:FORM SDEV\r\n')
        self._ser.write(':CALC3:DATA?\r\n')
      
    def read(self, time_to_wait):    
        while ( self._ser.inWaiting() <= 2 ):   
            pass       
        time.sleep(time_to_wait)    
        data = self._ser.read(self._ser.inWaiting()) 
        return data
    
    #===========================================================================
    # Returns a list with format [voltage,current]
    #===========================================================================

    def get_value(self, with_error = False):
        self.sample()
        self.get_mean()
        dmean = eval(self.read(self.configuration_file["Device"]["Configuration"]["WaitRead"]))
        if(with_error == True):    
            self.get_std()
            dstd = eval(self.read(self.configuration_file["Device"]["Configuration"]["WaitRead"]))
            return dmean, dstd
        else: 
            return dmean
        
    def get_voltage(self):
        self.sample()        
        self.get_mean()
        d = eval(self.read(0.1).split(",")[0])
        self.get_std()
        derr = eval(self.read(0.1).split(",")[0])
        print "Voltage = "+str(d)+"V +/- "+str(derr)+"V"    
        return d

            
    def get_current(self):
        self.sample()
        self.get_mean()
        d = eval(self.read(0.1).split(",")[1])/0.000001,
        self.get_std()
        derr = eval(self.read(0.1).split(",")[1])/0.000001,
        print "Current = "+str(d)+"uA +/- "+str(derr)+"uA" 


    #===========================================================================
    # One simple command to read out the status
    #===========================================================================

    def state(self):
        print "If the script stops here, the output is turned off\n."
        print "Output voltage: "
        # 2 : Read-Voltage
        self.get_voltage()
        print "Output current: "
        # 3 : Current
        self.get_current()

    def ramp_vsub(self, vset, unit):
        print "If the script stops here, the output is turned off\n."
	d = self.get_voltage()
	print "Ramping output from " + str(d) + "V to " + str(vset) + str(unit)
	while( (d-1) > vset ):	# Ramping down
            self.set_voltage(d-1,unit)
            time.sleep(0.5)
            d = self.get_voltage()
	while( (d+1) < vset ):	# Ramping up
            self.set_voltage(d+1,unit)
            time.sleep(0.5)        
            d = self.get_voltage()
	self.set_voltage(vset,unit)

 

