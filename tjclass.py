#
# ------------------------------------------------------------
# Christian Riegel
# CERN / University of Wuppertal
# 27/11/2015
# Functions for the TJ-INVESTIGATOR setup
# Using pyBAR and Basil
# ------------------------------------------------------------
#


import time
from bitarray import bitarray
from basil.dut import Dut

class TowerJazzSetup:

    def __init__(self, conf):
        self=Dut(conf)
        self.init()

    def vdd(p_vdd,p_limit=70):
        self['gpac'].set_current_limit('PWR0',p_limit,unit='mA')
        self['Vdd'].set_voltage(voltage, unit='V')
        self['Vdd'].set_enable(True)
        print("Vdd set to "+str(voltage)+". Current  is "+str(self['Vdd'].get_current(unit="mA"))+" mA".)

    def ramp(p_vdd,p_limit=70):
        voltage = self['Vdd'].get_voltage()
        if p_vdd > voltage:
            while (voltage + 0.1) < p_vdd:
                self.vdd(voltage+0.1,p_limit)
                time.sleep(0.5)
        else
            while (voltage - 0.1) > p_vdd:
                self.vdd(voltage-0.1,p_limit)
                time.sleep(0.5)

    def on():
        self['Vdd'].set_enable(True)

    def off():
        self['Vdd'].set_enable(False)

    def resetV(p_vreset=1.0):
        self['Vreset'].set_voltage(p_vreset, unit='V')
        print("Reset voltage set to: "+str(self['Vreset'].get_voltage())+" V")

    def setMatrix(p_matrix):
        self["Gpio"]["Matrix"] = p_matrix
        self["Gpio"].write()
        print("Selection matrix set to: "+str(self["Gpio"]["Matrix"]))

    def getMatrix():
        print("Selection matrix set to: "+str(self["Gpio"]["Matrix"]))

    def limitI(p_lim):
        self['gpac'].set_current_limit('PWR0',p_lim,unit='mA')
        print("Current limit of PWR0 set to "+str(p_lim)+" mA")

    def current():
        print("Current current is "+str(self['Vdd'].get_current(unit="mA")))

    def setReset(p_active):
	    if(p_active):
	    	self["Gpio"]["LED"] = ( self["Gpio"]["LED"] | bitarray('00001') )
	    else:
	    	self["Gpio"]["LED"] = ( self["Gpio"]["LED"] & bitarray('11110') )
	    self["Gpio"].write()
	    print("Reset set to: "+str(p_active))

    def setPulsing(p_active):
	    if(p_active):
	    	self["Gpio"]["LED"] = ( self["Gpio"]["LED"] | bitarray('00010') )
	    else:
	    	self["Gpio"]["LED"] = ( self["Gpio"]["LED"] & bitarray('11101') )
	    self["Gpio"].write()
	    print("Pulsing set to: "+str(p_active))

    def setLed(p_bit):
	    self["Gpio"]["LED"] = p_bit
	    self["Gpio"].write()

def setInjPulse(dut,Vlow,Vhigh,tdelay,twidth):
  dut['VPulse_low'].set_voltage(Vlow, unit='V')
  dut['VPulse_high'].set_voltage(Vhigh, unit='V')
  dut['Inj_Pulse'].DELAY = tdelay
  dut['Inj_Pulse'].WIDTH = twidth
  print("Pulse delay time set to: "+str(dut['Inj_Pulse'].DELAY)+" clock cycles")
  print("Pulse width time set to: "+str(dut['Inj_Pulse'].WIDTH)+" clock cycles") 
  print("Pulse repetitions set to: "+str(dut['Inj_Pulse'].REPEAT))

def startReset(dut, p_nrepeat):
  dut['Inj_Pulse'].REPEAT = p_nrepeat
  dut['Inj_Pulse'].EN = True
  dut['Inj_Pulse'].start()

def probeReset(p_sleep=1):
  for i in range(10):
    voltage = float(0.1+i*0.1)
    resetV(float(voltage))
    time.sleep(float(p_sleep))



#def ifolCurrents(dut):
#  print("\n\nReading the four currents:\n")
#  for i in range(4):
#	  print("Current I"+str(i)+" set to: "+str(icurrents[i])+" uA and read back: "+str(dut['Current'+str(i)].get_current(unit="uA"))+" uA\n")




