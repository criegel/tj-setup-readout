#
# ------------------------------------------------------------
# Christian Riegel
# CERN / University of Wuppertal
# 22/10/2015
# Main control script for using the TJ INVESTIGATOR
# Using pyBAR and Basil
# ------------------------------------------------------------
#

#### Parameter Header ###

vdd       = 1.80						# V
vreset    = 1.00						# V
vdelay    = 1.80						# V
vwidth    = 0.20						# V
dac0      = 0.32            # V
dacX      = 0.32            # V


tperiod   = 10 #40						  # clock cycles
tdelay    = 3 #12						  # clock cycles
twidth    = tperiod - tdelay
nrepeat   = 0               # [0-255], 0=infinite
autostart = True						# [0,1]

#########################

tj = Dut('test.yaml')
tj.init()

print("\n\nHardware Initialised.\n")

	# Selection of Investigator Mini-Matrix
setMatrix(2)

	# Preparing for later ramping of Vdd
tj['gpac'].set_current_limit('PWR0',40,unit='mA')		# Simulation: 34mA consumption!!
tj['Vdd'].set_enable(False)

	# This channel is setting VRESET
resetV(vreset)

	# Setting the DAC voltages to create the IFOL currents
tj['Vfol0'].set_voltage(dac0, unit='V')
tj['VfolX'].set_voltage(dacX, unit='V')

	# This channel sets VPULSE
#setInjPulse(vdelay,vwidth,tdelay,twidth)
setInjPulse(tj,vdelay,0.001,12,3)  # New set to cope with the longer reset needed

if(autostart):
	tj['Inj_Pulse'].start()
	print("\nPulse started.")

rampVdd(tj,5)

	# Remark: rampVdd() still has to be called in order to switch on the Investigator
print("\nScript executed.")
