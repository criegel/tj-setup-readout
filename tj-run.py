#
# ------------------------------------------------------------
# Christian Riegel
# CERN / University of Wuppertal
# 22/10/2015
# Main control script for using the TJ INVESTIGATOR
# Using pyBAR and Basil
# ------------------------------------------------------------
#

from tjfunctions import *
from Keithley import KeithleySMU2400Series
import yaml

#### Parameter Header ###

vdd       = 1.80						# V
vreset    = 1.00						# V
vdelay    = 1.80						# V
vwidth    = 0.20						# V
dac0      = 0.41                        # V
dacX      = 0.32                        # V


tperiod   = 10 #40						  # clock cycles
tdelay    = 3 #12						  # clock cycles
twidth    = tperiod - tdelay
nrepeat   = 0               # [0-255], 0=infinite
autostart = True						# [0,1]

#########################

tj = Dut('tj-setup.yaml')
tj.init()

print("\n\nTJ Initialised.\n")

#########################

#configuration_file = ''

#with open('config_keithley_tj.yaml', 'r') as file:
#    configuration_file = yaml.load(file)

#vsub = KeithleySMU2400Series(configuration_file)
#vsub.disable_output()

#vsub.set_voltage(0 ,unit='V')

#print("\n\nKeithley Initialised.\n")

#########################

	# Selection of Investigator Mini-Matrix
#setMatrix(tj,2)
setMatrix(tj,129)

	# Preparing for later ramping of Vdd
tj['gpac'].set_current_limit('PWR0',70,unit='mA')		# Simulation: 34mA consumption!!
tj['Vdd'].set_enable(False)

	# This channel is setting VRESET
resetV(tj,vreset)

	# Setting the DAC voltages to create the IFOL currents
tj['Vfol0'].set_voltage(dac0, unit='V')
tj['Vfol1'].set_voltage(dacX, unit='V')
tj['Vfol2'].set_voltage(dacX, unit='V')
tj['Vfol3'].set_voltage(dacX, unit='V')


	# This channel sets VPULSE
setInjPulse(tj,0.001,vdelay,200,50) #12,3

if(autostart):
	tj['Inj_Pulse'].start()
	print("\nPulse started.")

	# Remark: rampVdd() still has to be called in order to switch on the Investigator
print("\nScript executed.")

startReset(tj,0)

#setLed(tj,2)
#rampVdd(tj,5)

print("\n\n\n\n\nSELECT THE CORRECT MINIMATRIX!!\n\n\n\n")
