#
# ------------------------------------------------------------
# Christian Riegel
# CERN / University of Wuppertal
# Small script to read the four currents 
# 22/10/2015
# ------------------------------------------------------------
#



print("\n\nReading the four currents:\n")
	# These lines set the current sources => Set the correct values!!
for i in range(4):
	print("Current I"+str(i)+" set to: "+str(icurrents[i])+" uA and read back: "+str(me['Current'+str(i)].get_current(unit="uA"))+" uA\n")


